-- wireshark LUA dissector for the ITU-T T.62 protocol
-- (C) 2024 by Harald Welte <laforge@osmocom.org>
-- SPDX-License-Identifier: GPL-2.0+
--
-- Usage: Move this file to your "personal lua plugins" folder that
-- can be found in the Wireshark Help->About Wireshark->Folders tab
-- Windows: %APPDATA%\Wireshark\plugins.
-- Unix-like systems: ~/.local/lib/wireshark/plugins.

t62_protocol = Proto("T.62", "ITU-T T.62 Control Procedures for Teletex and Group 4 Facsimile Services")


local session_cri_names_long = {
	-- Table 4/T.62: CRI for session elements
	[0x0d] = "Command Session Start",
	[0x09] = "Command Session End",
	[0x19] = "Command Session Abort",
	[0x15] = "Command Session Change Control",
	[0x01] = "Command Session User Information",
	[0x0e] = "Response Session Start Positive",
	[0x0c] = "Response Session Start Negative",
	[0x0a] = "Response Session End Positive",
	[0x1a] = "Response Session Abort Positive",
	[0x16] = "Response Session Change Control Positive",
	[0x02] = "Response Session User Information",
}
local session_cri_names = {
	-- Table 4/T.62: CRI for session elements
	[0x0d] = "CSS",
	[0x09] = "CSE",
	[0x19] = "CSA",
	[0x15] = "CSCC",
	[0x01] = "CSUI",
	[0x0e] = "RSSP",
	[0x0c] = "RSSN",
	[0x0a] = "RSEP",
	[0x1a] = "RSAP",
	[0x16] = "RSCCP",
	[0x02] = "RSUI",
}
local hf_session_cri = ProtoField.uint8("t62.session.cri", "Command/Response Identifier", base.HEX, session_cri_names)
local hf_session_li = ProtoField.uint8("t62.session.li", "Length Identifier", base.DEC)

-- Table 7/T.62
local session_pi_pgi_names = {
	[0x00] = "Reserved for extension",
	[0x01] = "Session reference",
		[0x09] = "Terminal identifier of the called terminal",
		[0x0a] = "Terminal identifier of the calling terminal",
		[0x0b] = "Date and time",
		[0x0c] = "Additional session reference number",
	[0x02] = "Non-basic session capabilities",
		[0x0d] = "Miscellaneous session capabilities",
		[0x0e] = "Window size",
	-- No PGI associated with these PIs
		[0x08] = "Service identifier",
		[0x10] = "Session control functions",
		[0x11] = "Session termination parameter",
		[0x12] = "Inactivity timer",
		[0x14] = "Session service functions",
		[0x32] = "Reason",
	[0x41] = "Non-basic Teletex terminal capabilities",
		[0x49] = "Control character set",
		[0x4a] = "Teletex page format",
		[0x4b] = "Miscellaneous Teletex terminal capabilities",
	[0xc1] = "Session user data",
	[0xe0] = "Private use 0",
	[0xe1] = "Private use 1",
	[0xe2] = "Private use 2",
	[0xe3] = "Private use 3",
	[0xe4] = "Private use 4",
	[0xe5] = "Private use 5",
	[0xe6] = "Private use 6",
	[0xe7] = "Private use 7",
	[0xe8] = "Non-standardixed capabilities",
}
local hf_session_pi_pgi = ProtoField.uint8("t62.session.pi", "Session Parameter (Group) Identifier", base.HEX, session_pi_pgi_names)
local hf_session_pli = ProtoField.uint8("t62.session.pli", "Session Parameter (Group) Length Identifier", base.DEC)
local hf_session_pdata = ProtoField.bytes("t62.session.pdata", "Session Parameter Data")

local doc_cri_names_long = {
	-- Table 5/T.62: CRI for document command identifiers
	[0x2d] = "Command Document Start",
	[0x1d] = "Command Document Continue",
	[0x29] = "Command Document End",
	[0x19] = "Command Document Resynchronize",
	[0x39] = "Command Document Discard",
	[0x31] = "Comman Document Page Boundary",
	[0x3d] = "Command Document Capability List",
	[0x01] = "Command Document User Information",
	-- Table 6/T.62: CRI for document response identifiers
	[0x2a] = "Response Document End Potve",
	[0x1a] = "Response Document Resynchronize Positive",
	[0x3a] = "Response Document Discard Positive",
	[0x32] = "Response Document Page Boundary Positive",
	[0x30] = "Response Document Page Boundary Negative",
	[0x3e] = "Response Document Capability List Positive",
	[0x00] = "Resposne Documnet General Reject",
}
local doc_cri_names = {
	-- Table 5/T.62: CRI for document command identifiers
	[0x2d] = "CDS",
	[0x1d] = "CDC",
	[0x29] = "CDE",
	[0x19] = "CDR",
	[0x39] = "CDD",
	[0x31] = "CDPB",
	[0x3d] = "CDCL",
	[0x01] = "CDUI",
	-- Table 6/T.62: CRI for document response identifiers
	[0x2a] = "RDEP",
	[0x1a] = "RDRP",
	[0x3a] = "RDDP",
	[0x32] = "RDPBP",
	[0x30] = "RDPBN",
	[0x3e] = "RDCLP",
	[0x00] = "RDGR",
}
local hf_session_param = ProtoField.none("t62.session.param", "Document Parameter")
local hf_document_cri = ProtoField.uint8("t62.document.cri", "Command/Response Identifier", base.HEX, doc_cri_names)
local hf_document_li = ProtoField.uint8("t62.document.li", "Length Identifier", base.DEC)

local hf_document_data = ProtoField.bytes("t62.document.data", "Data")

-- Table 8/T.62
local doc_pi_pgi_names = {
	[0x20] = "Reserved fro extension",
	[0x21] = "Document linking",
		[0x09] = "Terminal identifier of the called terminal",
		[0x0a] = "Terminal identifier of the calling terminal",
		[0x0b] = "Date and time",
		[0x0c] = "Additional session reference number",
		[0x29] = "Document reference number",
		[0x2a] = "Checkpoint reference number",
	-- no PGI associated wih these PI
		[0x12] = "Inactivity timer",
		[0x28] = "Service interworking identifier",
		[0x29] = "Document reference number",
		[0x2a] = "Checkpoint reference number",
		[0x2c] = "Acceptance of CDCL parameters",
		[0x2d] = "Storage capacity negotiation",
		[0x2e] = "Receiving ability jeopardized",
		[0x2f] = "Reserved",
		[0x30] = "Document type identifier",
		[0x31] = "Reflect parameter values",
		[0x32] = "Reason",
	[0x40] = "Reserved for extension",
	[0x41] = "Non-basic Teletex terminal capabilities",
		[0x48] = "Graphic character set",
		[0x49] = "Control character set",
		[0x4a] = "Teletex page format",
		[0x4b] = "Miscellaneous Teletex terminal capabilities",
		[0x4d] = "Character box height",
		[0x4e] = "Character box width",
	[0xc1] = "Session user data",
	[0xe0] = "Private use 0",
	[0xe1] = "Private use 1",
	[0xe2] = "Private use 2",
	[0xe3] = "Private use 3",
	[0xe4] = "Private use 4",
	[0xe5] = "Private use 5",
	[0xe6] = "Private use 6",
	[0xe7] = "Private use 7",
	[0xe8] = "Non-standardized capabilities",
}
local hf_doc_param = ProtoField.none("t62.document.param", "Document Parameter")
local hf_doc_pi_pgi = ProtoField.uint8("t62.document.pi", "Document Parameter (Group) Identifier", base.HEX, doc_pi_pgi_names)
local hf_doc_pli = ProtoField.uint8("t62.document.pli", "Document Parameter (Group) Length Identifier", base.DEC)
local hf_doc_pdata = ProtoField.bytes("t62.document.pdata", "Document Parameter Data")

-- 5.7.2.10 Reason (session or document)
local reason_names = {
	[0x00] = "No specific reason stated (used for session or document reasons other than those listed)",
	[0x01] = "Temporarily unable to enter into, or to continue, a session (e.g. due to memory full or out of recording paper)",
	[0x02] = "Explicit text message only for use with RSSN",
	[0x03] = "Sequence error",
	[0x05] = "Local terminal error",
	[0x06] = "Unrecoverable procedural error",
}

-- 5.7.3.7 Document type identiifer
local doc_type_id_names = {
	[0x01] = "Operator document",
	[0x02] = "Control document",
	[0x03] = "Monitor document",
}

-- Table 11/T.62
local teletex_page_format_names = {
	[0x01] = "ISO A4, horizontal and vertical",
	[0x02] = "North American, horizontal and vertical",
	[0x84] = "ISO A4 extended (ISO standard 3535), vertical",
	[0x44] = "ISO A4 extended (ISO standard 3535), horizontal",
	[0x88] = "North American legal, vertical",
	[0x48] = "North American legal, horizontal",
	[0x03] = "ISO A4, horizontal and vertical (for use by Japanese Kanji and Chinese ideogram terminals)",
	[0x10] = "ISO B5, horizontal and vertical (for use by Japanese Kanji and Chinese ideogram terminals)",
	[0x20] = "ISO B4, horizontal and vertical (for use by Japanese Kanji and Chinese ideogram terminals)",
}

t62_protocol.fields = {
	hf_session_cri, hf_session_li,
	hf_session_param, hf_session_pi_pgi, hf_session_pli, hf_session_pdata,
	hf_document_cri, hf_document_li,
	hf_doc_param, hf_doc_pi_pgi, hf_doc_pli, hf_doc_pdata,
	hf_document_data,
}

function decode_one_session_pi_pgi(buffer, offset, pinfo, tree)
	local pi = buffer(offset, 1):uint()
	local p_li = buffer(offset+1, 1):uint()
	local subtree = tree:add(hf_session_param, buffer(offset, 2+p_li))
	subtree:append_text(" " .. session_pi_pgi_names[pi])
	subtree:add(hf_session_pi_pgi, buffer(offset, 1))
	subtree:add(hf_session_pli, buffer(offset+1, 1))
	subtree:add(hf_session_pdata, buffer(offset+2, p_li))
	return 2 + p_li
end

function decode_one_doc_pi_pgi(buffer, offset, pinfo, tree)
	local pi = buffer(offset, 1):uint()
	local p_li = buffer(offset+1, 1):uint()
	local subtree = tree:add(hf_doc_param, buffer(offset, 2+p_li))
	subtree:append_text(" " .. doc_pi_pgi_names[pi])
	subtree:add(hf_doc_pi_pgi, buffer(offset, 1))
	subtree:add(hf_doc_pli, buffer(offset+1, 1))
	subtree:add(hf_doc_pdata, buffer(offset+2, p_li))
	return 2 + p_li
end

function decode_t62_document(buffer, pinfo, tree)
	local d_cri = buffer(0, 1):uint()
	local d_li = buffer(1, 1):uint()
	local subtree = tree:add(t62_protocol, buffer(), "T.62 Document")

	pinfo.cols.info:append(" " .. doc_cri_names[d_cri])
	subtree:append_text(" " .. doc_cri_names[d_cri] .. " (" .. doc_cri_names_long[d_cri] .. ")")
	subtree:add(hf_document_cri, buffer(0, 1))
	subtree:add(hf_document_li, buffer(1, 1))

	-- document parameters
	if (d_li > 0) then
		local offset = 2
		while (offset <= buffer:len() and offset - 2 < d_li) do
			offset = offset + decode_one_doc_pi_pgi(buffer, offset, pinfo, subtree)
		end
	end

	-- user data only exists in CDUI
	if (d_cri == 0x01) then
		subtree:add(hf_document_data, buffer(2))
	end
end

function t62_protocol.dissector(buffer, pinfo, tree)
	pinfo.cols.protocol = t62_protocol.name

	local subtree = tree:add(t62_protocol, buffer(), "T.62 Session")
	local offset = 0

	local s_cri = buffer(0, 1):uint()
	local s_li = buffer(1, 1):uint()

	pinfo.cols.info:append(" " .. session_cri_names[s_cri])
	subtree:append_text(" " .. session_cri_names[s_cri] .. " (" .. session_cri_names_long[s_cri] .. ")")
	subtree:add(hf_session_cri, buffer(0, 1))
	subtree:add(hf_session_li, buffer(1, 1))

	if (s_li > 0) then
		-- session parameters
		local offset = 2
		while (offset <= buffer:len() and offset - 2 < s_li) do
			offset = offset + decode_one_session_pi_pgi(buffer, offset, pinfo, subtree)
		end
	end

	-- document layer data only exists in CSUI / RSUI
	if (s_cri == 0x01 or s_cri == 0x02) then
		decode_t62_document(buffer(2+s_li), pinfo, tree)
	end
end

function t62_protocol.init()
	local t70_dissectors = DissectorTable.get("t70.subdissector")
	t70_dissectors:add_for_decode_as(t62_protocol)
end
