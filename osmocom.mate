/***********************************************************************
 * MGCP
 ***********************************************************************/

/* MGCP is rather complex to match. Why?
   - the verb is only present in the request, bu not the response.  So by looking
     at the resposne you don't know whether it's a CRCX response or a MDCX one.
   - a request can specify wildcard endpoint, with the chosen endpoint only showing
     up in the response
   - one would actually want to treat all messages for one Connection as Gop
   - probably treat all Connections on same EP as Gog?
 */

Pdu mgcp_pdu Proto mgcp Transport udp/ip {
	Extract ip_addr From ip.addr;
	Extract port From udp.port;

	/* For some unknown reason the below fields are not actually extracted
	 * by wireshark - why is that ?!? */
	Extract mgcp_rsp_code From mgcp.rsp.rspcode;
	Extract mgcp_verb From mgcp.req.verb;
	Extract mgcp_endpoint From mgcp.req.endpoint;
	Extract mgcp_conn_id From mgcp.param.connectionid;
	Extract mgcp_spec_endp_id From mgcp.param.specificendpointid;
};

Gop mgcp_conn On mgcp_pdu Match (ip_addr, ip_addr, port, port, mgcp_conn_id) {
	Start (mgcp_rsp_code = 200, mgcp_spec_endp_id);
	Stop (mgcp_verb = "DLCX");
};


/***********************************************************************
 * A-bis RSL
 ***********************************************************************/

/* For RSL, we want to mark all messages related to one logical channel,
   from RSL CHAN ACT all the way to RF CHAN REL */

Pdu rsl_pdu Proto gsm_abis_rsl Transport gsm_ipa/tcp/ip {
	Extract ip_addr From ip.addr;
	Extract port From tcp.port;
	Extract rsl_cbits From gsm_abis_rsl.ch_no_Cbits;
	Extract rsl_tn From gsm_abis_rsl.ch_no_TN;
	Extract rsl_msg_dsc From gsm_abis_rsl.msg_dsc;
	Extract rsl_msg_type From gsm_abis_rsl.msg_type;
	Criteria Accept Strict (rsl_msg_dsc {4|1|63}); // DCHAN || RLL || IPA
};

/* somehow this definition for the Gop doesn't work, as ip_addr exists only one if source and dest are
 * identical */
Gop rsl_lchan On rsl_pdu Match (ip_addr, ip_addr, port, port, rsl_cbits, rsl_tn) {
	Start (rsl_msg_type = 33); // CHAN_ACT
	Stop (rsl_msg_type {36|51}); // CHAN_ACT_NACK || RF_CHAN_REL_ACK
};

/***********************************************************************
 * SCCP
 ***********************************************************************/

/* We don't really have to track SCCP connections; the SCCP dissector does that (assoc.id),
   but that is somehow broken (20200314)? */
Pdu sccp_pdu Proto sccp Transport m3ua/ip {
	Extract pc From m3ua.protocol_data_opc;
	Extract pc From m3ua.protocol_data_dpc;
	//Extract sccp_assoc_id From sccp.assoc.id;
	Extract sccp_lr From sccp.lr;
	Extract sccp_msg_type From sccp.message_type;
};

//Gop sccp_conn On sccp_pdu Match (pc, pc, sccp_assoc_id) {
Gop sccp_conn On sccp_pdu Match (pc, pc, sccp_lr) {
	Start (sccp_msg_type = "0x00000001"); // CR
	Stop (sccp_msg_type {"0x00000005"}); // RLC
};


/***********************************************************************
 * BSSAP
 ***********************************************************************/

/***********************************************************************
 * GTPv1-C
 ***********************************************************************/

/* incomplete as it can only track one of the two TEID, so we get all requests but not the responses */

Pdu gtp_pdu Proto gtp Transport udp/ip {
	Extract ip_addr From ip.addr;
	Extract port From udp.port;
	Extract gtp_msg_type From gtp.message;
	Extract teid From gtp.teid_cp;
	Extract teid From gtp.teid;
};

Gop gtp_ctx On gtp_pdu Match (ip_addr, ip_addr, port, port, teid) {
	Start (gtp_msg_type {16}); // CREATE PDP CTX
	Stop (gtp_msg_type {21}); // DELETE PDP CTX RESP
};

/***********************************************************************
 * GTPv2-C
 ***********************************************************************/

/* incomplete as it can only track one of the two TEID, so we get all requests but not the responses */

Pdu gtpv2_pdu Proto gtpv2 Transport udp/ip {
	Extract ip_addr From ip.addr;
	Extract port From udp.port;
	Extract gtpv2_msg_type From gtpv2.message_type;
	Extract teid From gtpv2.teid;
};

Gop gtpv2_ctx On gtpv2_pdu Match (ip_addr, ip_addr, port, teid) {
	Start (gtpv2_msg_type {33}); // CREATE SESSION RESP
	Stop (gtpv2_msg_type {37}); // DELETE SESSION RESP
};


/***********************************************************************
 * PFCP
 ***********************************************************************/

Pdu pfcp_pdu Proto pfcp Transport udp/ip {
	Extract ip_addr From ip.addr;
	Extract port From udp.port;
	Extract pfcp_msg_type From pfcp.msg_type;
	Extract seid From pfcp.seid;
};

Gop pfcp_ctx On pfcp_pdu Match (ip_addr, ip_addr, port, seid) {
	/* we cannot match the request, as it contains seid 0 */
	Start (pfcp_msg_type {51}); // Session Establishment Response
	Stop (pfcp_msg_type {55}); // Session Deletion Response
};

/***********************************************************************
 * S1AP
 ***********************************************************************/

Pdu s1ap_pdu Proto s1ap Transport sctp/ip {
	Extract ip_addr From ip.addr;
	Extract port From sctp.port;
	Extract s1ap_proc_code From s1ap.procedureCode;
	Extract s1ap_mme_id From s1ap.MME_UE_S1AP_ID;
	Extract s1ap_enb_id From s1ap.ENB_UE_S1AP_ID;
};

Gop s1ap_ctx On s1ap_pdu Match (ip_addr, ip_addr, port, port, s1ap_mme_id) {
	Start (s1ap_proc_code {9}); // InitialContextSetup
	Stop (s1ap_proc_code {23}); // UEContextRelease
};



Done;
