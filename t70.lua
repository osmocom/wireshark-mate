-- wireshark LUA dissector for the ITU-T T.70 protocol
-- (C) 2024 by Harald Welte <laforge@osmocom.org>
-- SPDX-License-Identifier: GPL-2.0+
--
-- Usage: Move this file to your "personal lua plugins" folder that
-- can be found in the Wireshark Help->About Wireshark->Folders tab
-- Windows: %APPDATA%\Wireshark\plugins.
-- Unix-like systems: ~/.local/lib/wireshark/plugins.

t70_protocol = Proto("T.70", "ITU-T T.70 Network-Independent Basic Transport Service for the Telematic services")

t70_subdissectors = DissectorTable.new("t70.subdissector", "T.70 payload subdissector", ftypes.NONE, base.NONE, t70_protocol)

local hf_net_block_hdr_len = ProtoField.uint8("t70.net_block_hdr_len", "Network Block Header Length", base.DEC)
local hf_net_block_hdr = ProtoField.bytes("t70.net_block_hdr", "Network Block Header")
local hf_net_block_hdr_m = ProtoField.bool("70.net_block_hdr.m", "M(ore data mark) bit", base.HEX, nil, 0x80)
local hf_net_block_hdr_q = ProtoField.bool("70.net_block_hdr.q", "Q(ualifier) bit", base.HEX, nil, 0x40)

local hf_src_ref = ProtoField.uint16("t70.src_ref", "Source Reference", base.HEX)
local hf_dst_ref = ProtoField.uint16("t70.dst_ref", "Destination Reference", base.HEX)

-- Fogure 15/T.70
local reject_cause_names = {
	[0x00] = "Reason not specified",
	[0x01] = "Function not implemented",
	[0x02] = "Invalid block",
	[0x03] = "Invalid parameter",
}
local hf_rej_cause = ProtoField.uint8("t70.rej_cause", "Reject Cause", base.HEX, reject_cause_names)
local hf_block_len = ProtoField.uint8("t70.block_len", "Block Length", base.DEC)
local block_types = {
	[0xe0] = "TCR",
	[0xd0] = "TCA",
	[0x80] = "TCC",
	[0x70] = "TBR",
	[0xf0] = "TDT",
}
local block_types_long = {
	[0xe0] = "Transport Connection Request",
	[0xd0] = "Transport Connection Accept",
	[0x80] = "Transport Connection Clear",
	[0x70] = "Transport Block Reject",
	[0xf0] = "Transport Data",
}
local hf_block_type = ProtoField.uint8("t70.block_type", "Block Type", base.HEX, block_types, 0xff)
local hf_data = ProtoField.bytes("t70.data", "Data")

local tcr_tca_param_names = {
	[0xc0] = "Transport data block size", -- Figure 11/T.70
	[0xc1] = "Calling address", -- Figure 10/T.70 
	[0xc2] = "Called address", -- Fiure 10/T.70
}

local tcc_param_names = {
	[0xc0] = "Additional clearing information",
}

local tbr_param_names = {
	[0xc1] = "Rejected block",
}

-- Figure 13/T.70
local clearing_cause_names = {
	[0x00] = "Reason not specified",
	[0x01] = "Terminal occupied",
	[0x02] = "Terminal out of order",
	[0x03] = "Address unknown",
}
local hf_clearing_cause = ProtoField.uint8("t70.clearing_cause", "Clearing Cause", base.HEX, clearing_cause_names)

local hf_tsdu_end_mark = ProtoField.bool("t70.tsdu_end_mark", "TSDU end mark", base.HEX, nil, 0x80)

local hf_param = ProtoField.none("t70.param", "Parameter")
local hf_param_type = ProtoField.uint8("t70.param.type", "Parameter Type", base.HEX)
local hf_param_len = ProtoField.uint8("t70.param.len", "Parameter Length", base.DEC)
local hf_param_data = ProtoField.bytes("t70.param.data", "Parameter Data")

t70_protocol.fields = {
	hf_net_block_hdr_len, hf_net_block_hdr, hf_net_block_hdr_m, hf_net_block_hdr_q, hf_tsdu_end_mark,
	hf_src_ref, hf_dst_ref, hf_rej_cause, hf_block_len, hf_block_type, hf_data, hf_clearing_cause,
	hf_param, hf_param_type, hf_param_len, hf_param_data,
}

function decode_one_param(buffer, offset, pinfo, tree, names)
	local pt = buffer(offset+0, 1):uint()
	local pl = buffer(offset+1, 1):uint()
	local subtree = tree:add(hf_param, buffer(offset+2, pl))
	subtree:append_text(string.format(": 0x%02x %s (len=%u)", pt, names[pt], pl))
	local ti = subtree:add(hf_param_type, buffer(offset+0, 1))
	ti:append_text(string.format(": %s", names[pt]))
	subtree:add(hf_param_len, buffer(offset+1, 1))
	subtree:add(hf_param_data, buffer(offset+2, pl))
	return 2 + pl
end

function dissect_block(buffer, pinfo, tree, maintree)
	local blen = buffer(0, 1):uint()
	local btype = buffer(1, 1):uint()

	pinfo.cols.info = block_types[btype]
	tree:append_text(" " .. block_types[btype] .. " (" .. block_types_long[btype] .. ")")

	tree:add(hf_block_len, buffer(0, 1))
	tree:add(hf_block_type, buffer(1, 1))

	if (btype == 0xe0) then		-- TCR (Section 5.5.4)
		tree:add(hf_src_ref, buffer(4, 2))
		-- parameters (calling, called, block size)
		local offset = 7
		while (offset < buffer:len()) do
			offset = offset + decode_one_param(buffer, offset, pinfo, tree, tcr_tca_param_names)
		end
	elseif (btype == 0xd0) then	-- TCA (Section 5.5.5)
		tree:add(hf_dst_ref, buffer(2, 2))
		tree:add(hf_src_ref, buffer(4, 2))
		-- parameters (calling, called, block size)
		local offset = 7
		while (offset < buffer:len()) do
			offset = offset + decode_one_param(buffer, offset, pinfo, tree, tcr_tca_param_names)
		end
	elseif (btype == 0x80) then	-- TCC (Section 5.5.6)
		tree:add(hf_dst_ref, buffer(2, 2))
		tree:add(hf_src_ref, buffer(4, 2))
		tree:add(hf_clearing_cause, buffer(6, 1))
		-- parameters (addl clearing info)
		local offset = 7
		while (offset < buffer:len()) do
			offset = offset + decode_one_param(buffer, offset, pinfo, tree, tcc_param_names)
		end
	elseif (btype == 0x70) then	-- TBR (Section 5.5.7)
		tree:add(hf_dst_ref, buffer(2, 2))
		tree:add(hf_rej_cause, buffer(4, 1))
		-- parameters (rejected block [mandatory])
		local offset = 5
		while (offset < buffer:len()) do
			offset = offset + decode_one_param(buffer, offset, pinfo, tree, tbr_param_names)
		end
	elseif (btype == 0xf0) then	-- TDT (Section 5.5.8)
		-- TSDU end mark
		tree:add(hf_tsdu_end_mark, buffer(2, 1))
		-- data
		if (not t70_subdissectors:try(0, buffer(3):tvb(), pinfo, maintree)) then
			tree:add(hf_data, buffer(3))
		end
	else
		-- unknown
	end
end

function t70_protocol.dissector(buffer, pinfo, tree)
	pinfo.cols.protocol = t70_protocol.name

	local subtree = tree:add(t70_protocol, buffer(), "T.70")
	local net_bhl = buffer(0, 1):uint()

	-- 2-byte network block header, see Figure 4/T.70
	subtree:add(hf_net_block_hdr_len, buffer(0, 1))
	subtree:add(hf_net_block_hdr, buffer(1, net_bhl))
	subtree:add(hf_net_block_hdr_m, buffer(1, 1))
	subtree:add(hf_net_block_hdr_q, buffer(1, 1))

	-- actual block
	dissect_block(buffer(1+net_bhl), pinfo, subtree, tree)
end

function t70_protocol.init()
	local x75_dissectors = DissectorTable.get("x75.subdissector")
	x75_dissectors:add_for_decode_as(t70_protocol)
end
